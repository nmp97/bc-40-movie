import React from 'react';
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemMovie({ data }) {
  return (
    <div>
      <Card
        hoverable
        cover={<img className="h-56 object-cover object-top" alt="example" src={data.hinhAnh} />}
      >
        <Meta
          className="h-20"
          title="Europe Street beat"
          description={
            <NavLink
              to={`./detail/${data.maPhim}`}
              className={'px-5 py-2 border-2 mt-2 border-red-500 rounded'}
            >
              Xem ngay
            </NavLink>
          }
        />
      </Card>
    </div>
  );
}
