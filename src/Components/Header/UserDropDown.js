import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
// const items = [
//   {
//     label: <a href="https://www.antgroup.com">1st menu item</a>,
//     key: '0',
//   },
//   {
//     label: <a href="https://www.aliyun.com">2nd menu item</a>,
//     key: '1',
//   },
//   {
//     type: 'divider',
//   },
//   {
//     label: 'Đăng xuất',
//     key: '3',
//   },
// ];
const UserDropdown = ({ user, logoutBtn }) => (
  <Dropdown
    menu={{
      items: [
        { label: logoutBtn, key: '1' },
        {
          label: <span>Cập nhật tài khoản?</span>,
          key: '2',
        },
      ],
    }}
    trigger={['click']}
  >
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        {user.hoTen}
        <DownOutlined />
      </Space>
    </a>
  </Dropdown>
);
export default UserDropdown;
