import React from 'react';
import { NavLink } from 'react-router-dom';
import UserMenu from '../UserMenu';

export default function HeaderDesktop() {
  return (
    <div className="h-20 shadow w-full">
      <div className="container mx-auto h-full flex items-center justify-between px-5">
        <NavLink to="/">
          <span className="font-medium text-2xl animate-bounce">CycleFlix</span>
        </NavLink>
        <UserMenu />
      </div>
    </div>
  );
}
