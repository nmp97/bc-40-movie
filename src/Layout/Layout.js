import React from 'react';
import Header from '../Components/Header';
import Footer from '../Pages/Footer';

export default function Layout({ Component }) {
  console.log(Component);
  return (
    <div className="min-h-screen flex flex-col space-y-10">
      <Header />
      <div className="flex-grow">
        <Component />
      </div>
      <Footer />
    </div>
  );
}
