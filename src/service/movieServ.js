import axios from 'axios';
import { BASE_URL, configHeader, http } from './config';

export const movieServ = {
  getMovieList: () => {
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`,
    //   method: 'GET',
    //   headers: configHeader(),
    // });
    return http.get(`${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01`);
  },

  getMovieByTheater: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01`,
      method: 'GET',
      headers: configHeader(),
    });
  },

  getDetailMovie: (maPhim) => {
    return http.get(`api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
};
