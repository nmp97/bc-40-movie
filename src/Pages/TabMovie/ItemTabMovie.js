import moment from 'moment/moment';
import React from 'react';

export default function ItemTabMovie({ phim }) {
  return (
    <div className="p-5 flex space-x-10">
      <img src={phim.hinhAnh} alt="" className="w-36 h-36" />
      <div>
        <h3 className="font-medium text-xl">{phim.tenPhim}</h3>
      </div>
      <div className="grid grid-cols-2 gap-5">
        {phim.lstLichChieuTheoPhim.splice(0, 9).map((item, index) => {
          return (
            <span className="rounded p-2 bg-red-500 text-white font-medium" key={index}>
              {moment(item.ngayChieuGioChieu).format('DD/MM/YYYY ~ hh:mm a')}
            </span>
          );
        })}
      </div>
    </div>
  );
}
