import React, { useEffect, useState } from 'react';
import { movieServ } from '../../service/movieServ';
import { Tabs } from 'antd';
import ItemTabMovie from './ItemTabMovie';

export default function TabMovie() {
  const [heThongRap, setHeThongRap] = useState([]);

  const onChange = (key) => {
    console.log(key);
  };

  let renderHeThongRap = () => {
    return heThongRap.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: <img className="h-16" src={rap.logo} alt="" />,
        children: (
          <Tabs
            style={{ height: 500 }}
            defaultActiveKey="1"
            items={rap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: <div>{cumRap.tenCumRap}</div>,
                children: (
                  <div style={{ height: 500 }}>
                    {cumRap.danhSachPhim.map((item, index) => {
                      return <ItemTabMovie phim={item} key={index} />;
                    })}
                  </div>
                ),
              };
            })}
            tabPosition="left"
            onChange={onChange}
          />
        ),
      };
    });
  };

  const items = [
    {
      key: '1',
      label: `Tab 1`,
      children: `Content of Tab Pane 1`,
    },
  ];

  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <Tabs
        style={{ height: 500 }}
        defaultActiveKey="1"
        items={renderHeThongRap()}
        tabPosition="left"
        onChange={onChange}
      />
    </div>
  );
}
