import React from 'react';
import Header from '../Components/Header';
import ListMovie from './ListMovie';
import TabMovie from './TabMovie/TabMovie';

export default function HomePage() {
  return (
    <div className="space-y-10">
      <Header />
      <ListMovie />
      <TabMovie />
      <br />
    </div>
  );
}
