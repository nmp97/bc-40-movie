import React, { useEffect } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import { movieServ } from '../../service/movieServ';
import { useState } from 'react';
import { Progress } from 'antd';

export default function DetailPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    let fetDetail = async () => {
      try {
        let result = await movieServ.getDetailMovie(id);
        setMovie(result.data.content);
        console.log('result', result);
      } catch (error) {
        console.log('err', error);
      }
    };
    fetDetail();
  }, []);

  return (
    <div className="container">
      <div className=" flex space-x-10">
        <img src={movie.hinhAnh} alt="" className="w-1/3 mb-6" />
        <div className="space-y-5">
          <h2 className="font-bold text-xl">{movie.tenPhim}</h2>
          <h2>{movie.moTa}</h2>
          <p className="my-5">Đánh giá:</p>
          <Progress percent={movie.danhGia * 10} size="small" />
        </div>
      </div>
      <NavLink
        className="rounded px-5 py-2 text-white font-medium bg-red-600"
        to={`/booking/${id}`}
      >
        Mua vé
      </NavLink>
    </div>
  );
}
