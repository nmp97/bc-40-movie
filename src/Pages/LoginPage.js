import React from 'react';
import { Button, Form, Input, message } from 'antd';
import { userServ } from '../service/userService';
import { localUserServ } from '../service/localService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { USER_LOGIN } from '../redux/constants/userConstant';
import Lottie from 'lottie-react';
import bg_animate from '../asset/login.json';
import { setLoginAction, setLoginActionServ } from '../redux/action/UserAction';

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log('Success:', values);
    userServ
      .postLogin(values)
      .then((res) => {
        message.success('Login thành công');
        localUserServ.set(res.data.content);
        dispatch(setLoginAction(res.data.content));
        navigate('/');
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        message.error('Login thất bại');
      });
  };

  const onFinishThunk = (value) => {
    let onSuccess = () => {
      message.success('Login thành công');
      localUserServ.set(value);
      navigate('/');
    };
    dispatch(setLoginActionServ(value, onSuccess));
    console.log(value);
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className="h-screen w-screen flex bg-orange-500 justify-center items-center">
      <div className="mx-auto p-5 bg-white rounded container flex">
        <div className="w-1/2 h-full">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="w-1/2 h-full">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: '100%',
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: 'Please input your taiKhoan!',
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: 'Please input your matKhau!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            ></Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
              className="flex justify-center items-center"
            >
              <Button
                className="bg-orange-500 hover:text-white hover:border=hidden"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
