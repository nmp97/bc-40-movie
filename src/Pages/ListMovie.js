import React, { useEffect, useState } from 'react';
import ItemMovie from '../Components/ItemMovie';
import { movieServ } from '../service/movieServ';

export default function ListMovie() {
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    movieServ
      .getMovieList()
      .then((res) => {
        setMovie(res.data.content);
        console.log(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container p-5 grid grid-cols-4 justify-center gap-4">
      {movie.map((item) => {
        return <ItemMovie data={item} key={item.maPhim} />;
      })}
    </div>
  );
}
