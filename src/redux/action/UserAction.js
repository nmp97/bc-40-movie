import { userServ } from '../../service/userService';
import { USER_LOGIN } from '../constants/userConstant';
import { message } from 'antd';
import { localUserServ } from '../../service/localService';

export const setLoginAction = (value) => {
  return {
    type: USER_LOGIN,
    payload: value,
  };
};

export const setLoginActionServ = (value, onCompleted) => {
  return (dispatch) => {
    userServ
      .postLogin(value)
      .then((res) => {
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        localUserServ.set(res.data.content);
        onCompleted();

        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
